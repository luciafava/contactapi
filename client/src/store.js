import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    contacts: [],
    newContact: '',
  },
  getters: {
    newContact: state => state.newContact,
    contacts: state => state.contacts,
    /*contact: function (state) {
      return state.contact;
    }*/
    getContactById: (state) => (id) => {
      return state.contact.find(contact => contact.id === id)
    }
  },
  mutations: {
    SET_CONTACTS (state, contacts) {
      state.contacts = contacts
    },
    SET_CONTACT (state, contact) {
      state.contact = contact
    },
    ADD_CONTACT: (state, contact) => {
      state.contacts.push(contact)
    },
    CLEAR_NEW_CONTACT (state) {
      state.newContact = ''
    }
  },
  actions: {
    loadContacts ({ commit }) {
      axios
        .get('http://localhost:3000/contacts')
        .then(r => r.data)
        .then(contacts => {
        commit('SET_CONTACTS', contacts)
        })
    },
    clearNewContact ({ commit }) {
      commit('CLEAR_NEW_CONTACT')
    }
  }

})
