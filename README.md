# contactapi

Simple Contact Manager app demonstrating Vue.js/Vuex working with a REST API.

## Server

Built with:

* json-server(web server)

### Getting started

cd json-server

json-server --watch db.json

These instructions assume json-server is running on `localhost:3000`.

### Requirements

* node >= 8.11.2
* npm >= 6.1.0
* vue >= 3.0.0

# Client

Built with:

* Vue.js (UI framework)
* Vuex (state management)
* vue-router (routing)
* vue-cli (tooling)
* axios (HTTP client)

### Getting started

cd client

# Run dev server
npm run serve

